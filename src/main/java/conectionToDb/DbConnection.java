package conectionToDb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbConnection {
  private static String url;
  private static  String login;
  private static  String password;

  private static Connection connection = null;
  private DbConnection(){}

  public static Connection getConnection(){
    FileInputStream fis;
    Properties property = new Properties();
    try {
      fis = new FileInputStream("src/main/resources/config.properties");
      property.load(fis);
      url = property.getProperty("db.url");
      login = property.getProperty("db.login");
      password = property.getProperty("db.password");
    } catch (FileNotFoundException e2) {
      e2.printStackTrace();
    } catch (IOException e2) {
      System.err.println("Не знайдено файл за заданим шляхом");;
    }
  if(connection ==null){
    try{
      connection=DriverManager.getConnection(url,login,password);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
  return  connection;
  }

}
