package transformer;

import Annotation.Column;
import Annotation.PrimaryKeyComposite;
import Annotation.Table;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer <T> {
private final Class<T> toTransformermer;
 public Transformer(Class<T> toTransformermer){ this.toTransformermer=toTransformermer;}

 public Object fromResultSetToEntity(ResultSet rs) throws SQLException{
   Object entity=null;
   try{
     entity = toTransformermer.getConstructor().newInstance();
     if(toTransformermer.isAnnotationPresent(Table.class)){
       Field[] fields=toTransformermer.getDeclaredFields();
       for(Field field: fields){
         if(field.isAnnotationPresent(Column.class)){
           Column column= (Column) field.getAnnotation(Column.class);
           String name=column.name();
           int length =column.length();
           field.setAccessible(true);
           Class fieldType = field.getType();
           if(fieldType==String.class){
             field.set(entity,rs.getString(name));
           }else if(fieldType == Integer.class){
             field.set(entity,rs.getInt(name));
           }else if(fieldType == Date.class){
             field.set(entity,rs.getDate(name));
           }
         }else if(field.isAnnotationPresent(PrimaryKeyComposite.class)){
           field.setAccessible(true);
           Class fieldType=field.getType();
           Object FK=fieldType.getConstructor().newInstance();
           field.set(entity,FK);
           Field[] fieldsInner = fieldType.getDeclaredFields();
           for(Field fieldInner:fieldsInner){
             if(fieldInner.isAnnotationPresent(Column.class)){
               Column column = (Column) fieldInner.getAnnotation(Column.class);
               String name=column.name();
               int length =column.length();
               fieldInner.setAccessible(true);
               Class fieldInnerType = fieldInner.getType();
               if(fieldInnerType==String.class){
                 fieldInner.set(FK,rs.getString(name));
               }else if(fieldInnerType == Integer.class){
                 fieldInner.set(FK,rs.getInt(name));
               }else if(fieldInnerType == Date.class){
                 fieldInner.set(FK,rs.getDate(name));
               }
             }
           }
         }
       }
     }
   } catch (IllegalAccessException e) {
     e.printStackTrace();
   } catch (InstantiationException e) {
     e.printStackTrace();
   } catch (NoSuchMethodException e) {
     e.printStackTrace();
   } catch (InvocationTargetException e) {
     e.printStackTrace();
   }
   return entity;
 }
}
