package dao;

import java.sql.SQLException;
import java.util.List;

public interface BaseDao <T, ID> {
  List <T> findAll() throws SQLException;
  /*T findById(ID id);
  int create (T entity);
  int update (T entity);
  int delete (ID id);*/
}
