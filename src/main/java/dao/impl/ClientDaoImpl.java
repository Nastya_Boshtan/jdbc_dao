package dao.impl;

import com.mysql.cj.jdbc.ConnectionGroupManager;
import conectionToDb.DbConnection;
import dao.ClientDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Client;
import transformer.Transformer;

public class ClientDaoImpl implements ClientDao {

  private static final String FIND_ALL="Select * From client";

  public List<Client> findAll() throws SQLException {
    List <Client> clients = new ArrayList<Client>();
    Connection connection = DbConnection.getConnection();
    try(Statement statement=connection.createStatement()){
      try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
        while(resultSet.next()){
          clients.add((Client) new Transformer(Client.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    for()
    System.out.println(clients.get(1));
    return clients;
  }
}
