package model;

import Annotation.Column;
import Annotation.PrimaryKey;
import Annotation.Table;

@Table(name="client")
public class Client {

  @PrimaryKey
  @Column(name="id")
  private int id;
@Column(name="Surname",length = 25)
  private String surname;
@Column(name="Name",length = 25)
  private String name;
@Column(name="Middle_name",length = 25)
  private String middleName;
@Column(name="Country",length = 15)
  private String country;
@Column(name="Street",length = 15)
  private String street;
@Column(name="House_number",length = 6)
  private String houseNumber;
@Column(name="Phone",length = 20)
  private String phone;

public Client(){}

public Client(int id, String surname, String name, String middleName,String country, String street, String houseNumber, String phone){
  this.id=id;
  this.surname=surname;
  this.name=name;
  this.middleName=middleName;
  this.country=country;
  this.street=street;
  this.houseNumber=houseNumber;
  this.phone=phone;
}

  public int getId() { return id;}

  public void setId(int id) { this.id = id; }

  public String getSurname() {return surname; }

  public void setSurname(String surname) { this.surname = surname; }

  public String getName() {  return name; }

  public void setName(String name) { this.name = name; }

  public String getMiddleName() {return middleName;}

  public void setMiddleName(String middleName) {  this.middleName = middleName; }

  public String getCountry() { return country; }

  public void setCountry(String country) {this.country = country; }

  public String getStreet() { return street; }

  public void setStreet(String street) { this.street = street; }

  public String getHouseNumber() { return houseNumber; }

  public void setHouseNumber(String houseNumber) { this.houseNumber = houseNumber;}

  public String getPhone() { return phone;}

  public void setPhone(String phone) { this.phone = phone; }

  @Override
  public String toString() {
    return "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", middleName='" + middleName + '\'' +
        ", country='" + country + '\'' +
        ", street='" + street + '\'' +
        ", houseNumber='" + houseNumber + '\'' +
        ", phone='" + phone;
  }


}
